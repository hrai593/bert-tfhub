from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import tensorflow_hub as hub
from utils import prepare_data, modeling
import numpy as np


class BertModel(object):
    def __init__(self, saved_model_path, session = None):

        if session:
            self.session = session
        else:
            self.session = tf.Session()


        bert_config_file = saved_model_path+"/bert_config.json"

        self.init_checkpoint = saved_model_path+"/bert_model.ckpt"

        self.graph = tf.get_default_graph()

        saver = tf.train.import_meta_graph(saved_model_path+"/bert_model.ckpt.meta")
        saver.restore(self.session, self.init_checkpoint)






        #self.session.run(tf.global_variables_initializer())
        #self.session.run(tf.tables_initializer())




    #def get_weights():

model = BertModel("/home/hrai/Downloads/uncased_L-12_H-768_A-12")