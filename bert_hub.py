from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import tensorflow_hub as hub
from utils import prepare_data
import numpy as np


class BertModel(object):
    def __init__(self, hub_module_url, session = None):

        print("Loading BERT module")
        self.hub_module = hub.Module(hub_module_url, trainable=False)
        print("Done Loading")

        if session:
            self.session = session
        else:
            self.session = tf.Session()

        self.session.run(tf.global_variables_initializer())
        self.session.run(tf.tables_initializer())



    def get_weights(self, texts):
        inputs = prepare_data.prepare_examples(texts)

        bert_outputs = self.hub_module(
                inputs=inputs,
                signature="tokens",
                as_dict=True)




        output = bert_outputs["pooled_output"]

        return np.array(self.session.run(output))


    def run_batch(self, texts):
        return self.get_weights(texts)


    def run_sentence(self, sentence):
        return self.get_weights([sentence])


    def run_interactive(self):



        print("Running BERT interactively. Type 'exit' to exit.")
        while(True):
            input_sentence = input("Enter a sentence : ")
            if(input_sentence == 'exit'):
                print("Exiting interactive mode")
                break

            else:
                vectors = self.run_sentence(input_sentence)
                print(vectors)
                print("Shape : {}".format(vectors.shape))






