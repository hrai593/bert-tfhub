import numpy as np
from bert_hub import BertModel
import time

"""
List tasks to run using BERT
"""

start_time = time.time()
model = BertModel("https://tfhub.dev/google/bert_uncased_L-24_H-1024_A-16/1")
end_time = time.time()

print("Loaded in {} seconds.".format(end_time - start_time))


def run_srl():
    return None

def run_classifier():
    return None


def run_demo():

    model.run_interactive()


if __name__ == '__main__':
    run_demo()