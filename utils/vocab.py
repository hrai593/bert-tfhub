import itertools


class Vocab(object):
    def __init__(self, data):
        self.data = data
        self.vocabulary = None

    @classmethod
    def fromList(cls, texts):
        words = itertools.chain(*texts)
        words_set = set(sorted(words))
        return dict([(w,i+1) for i,w in enumerate(words_set)])


    @classmethod
    def fromFile(cls, filename):
        words = []
        with open(filename,'r') as vocab_file:
            lines = vocab_file.readlines()
            for line in lines:
                line = line.rstrip()
                word, _ = line.split('\t')
                words.append(word)


        return dict([(w,i+1) for i,w in enumerate(words)])






