import numpy as np
from nltk.tokenize import word_tokenize
from utils.vocab import Vocab

def text_to_ids(text, vocab):
    return [vocab[w] for w in text]

def pad_sequence(text, pad_token, max_sequence_length):
    return text+[pad_token]*(max_sequence_length - len(text))

def text_mask(text_ids):
    return [1 if text_id>0 else 0 for text_id in text_ids]

def text_segment(text_ids):
    return [0]*len(text_ids)


def convert_example(text, max_sequence_length, pad_token, vocab):

    text = pad_sequence(text, pad_token, max_sequence_length)
    text_ids = text_to_ids(text, vocab)
    text_masks =  text_mask(text_ids)
    text_segments = text_segment(text_ids)

    return (text_ids,text_masks,text_segments)

def convert_examples(texts, vocab):

    input_ids = []
    input_mask = []
    segment_ids = []
    pad_token = '<PAD>'
    vocab[pad_token] = 0

    max_sequence_length = max(len(text) for text in texts)

    for text in texts:
        text_ids, text_masks, text_segments = convert_example(text, max_sequence_length, pad_token,vocab)
        input_ids.append(text_ids)
        input_mask.append(text_masks)
        segment_ids.append(text_segments)


    return dict(input_ids = input_ids, input_mask = input_mask, segment_ids = segment_ids)


def prepare_examples(texts):

    tokenized_texts = [word_tokenize(text) for text in texts]
    modified_texts = [["CLS"]+text+['SEP'] for text in tokenized_texts]

    vocab = Vocab.fromList(modified_texts)

    return convert_examples(modified_texts, vocab)







